/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.prweb.item.Auction;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author exia
 */
@WebServlet(name = "Refresh", urlPatterns = {"/refresh"})
public class Refresh extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //Build JSON
        JSONObject returnedValue = new JSONObject();
        JSONArray data = new JSONArray();
        List<Auction> auctions = Auction.getAll();
        for (Auction auction : auctions){
            JSONObject anAuction = new JSONObject();
            anAuction.put("id", auction.getId());
            anAuction.put("author", auction.getAuthor());
            anAuction.put("title", auction.getTitle());
            anAuction.put("body", auction.getBody());
            anAuction.put("category_name", auction.getCategory().getName());
            anAuction.put("category_id", auction.getCategory().getId());
            data.put(anAuction);
        }
        returnedValue.put("data", data);
        //Send JSON
        PrintWriter printer = response.getWriter();
        response.setContentType("text/html;charset=UTF-8");
        printer.print(returnedValue.toString());

    }


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
