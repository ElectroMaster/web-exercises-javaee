/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author exia
 */
public class Utilities {
    public static void forwardScreen(HttpServletRequest request, HttpServletResponse response,
                    String fileName) {
        try {
            response.setContentType("text/html;charset=utf-8");
            RequestDispatcher dispatcher = request.getRequestDispatcher(fileName + ".jsp");
            response.setHeader("Content-Type", "text/html; charset=utf-8");
            dispatcher.include(request, response);
            
        } catch(IOException ex){
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        } catch(ServletException ex){
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void addDriver(){
        try{
            Class.forName("org.postgresql.Driver");
        }catch (ClassNotFoundException ex){
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, "Resource indisponible", ex);
        }
    }
    
    public static Connection getConnection(){
        String dbProtocol = "jdbc:postgresql";
        String dbHost = "127.0.0.1";
        String dbPort = "5432";
        String dbName = "prweb";
        String dbURL = dbProtocol+"://"+dbHost+":"+dbPort+"/"+dbName;
        
        String dbUser="athena";
        String dbPass="123456";
        
        Connection Cnx = null;
        try{
            Cnx = DriverManager.getConnection(dbURL, dbUser, dbPass);
        } catch(SQLException ex){
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Cnx;
    }
}
