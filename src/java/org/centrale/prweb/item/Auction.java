/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb.item;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import org.centrale.prweb.Utilities;

/**
 *
 * @author exia
 */
public class Auction {

    private Long id;
    private String title;
    private String author;
    private String body;
    private Category category;

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getBody() {
        return body;
    }

    public Category getCategory() {
        return category;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
    
    private static Auction populate(ResultSet rs){
        Auction auction = new Auction();
        try{
            auction.setId(rs.getLong("id"));
            auction.setTitle(rs.getString("Title"));
            auction.setAuthor(rs.getString("Author"));
            auction.setBody(rs.getString("body"));
            Category categ = new Category();
            categ.setId(rs.getLong("category_id"));
            categ.setName(rs.getString("Name"));
            auction.setCategory(categ);
        } catch(SQLException ex){
            Logger.getLogger(Auction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return auction;
    }
    
    
    public static void remove(Long value) {
        try {
            Connection cnx = Utilities.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("DELETE FROM Item WHERE Item.id=?");
            stmt.setLong(1, value);
            stmt.executeUpdate();
            cnx.close();
        }catch(SQLException ex){
            Logger.getLogger(Auction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Long create(String title, String author, String body, Long categ_id){
        Long lastInsertedId = null;
        try{
            Connection cnx = Utilities.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("INSERT INTO Item(Title, Author, Body, Category_id) VALUES (?, ?, ?, ?)", 
                    java.sql.Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, title);
            stmt.setString(2, author);
            stmt.setString(3, body);
            stmt.setLong(4, categ_id);
//            stmt.setLong(4, category_id);
            stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();
            if (rs.next()){
                lastInsertedId = rs.getLong(1);
            }
            cnx.close();
        } catch (SQLException ex){
            Logger.getLogger(Auction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lastInsertedId;
    }
    
    public static List<Auction> getAll(){
        List<Auction> auctions = new ArrayList<>();
        try{
            Connection cnx = Utilities.getConnection();
//            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM Item ORDER BY id");
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM Item LEFT JOIN Category ON Item.category_id = Category.id ORDER BY Item.id;");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                Auction auction = populate(rs);
                auctions.add(auction);
            }
            cnx.close();
        } catch(SQLException ex){
            Logger.getLogger(Auction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return auctions;
    }
}
