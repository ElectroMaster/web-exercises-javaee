/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb.item;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.centrale.prweb.Utilities;

/**
 *
 * @author exia
 */
public class Category {
    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
      
    private static Category populate(ResultSet rs){
        Category category = new Category();
        try{
            category.setId(rs.getLong("id"));
            category.setName(rs.getString("Name"));
        } catch(SQLException ex){
            Logger.getLogger(Category.class.getName()).log(Level.SEVERE, null, ex);
        }
        return category;
    }
    public static List<Category> getAll(){
        List<Category> categories = new ArrayList<>();
        try{
            Connection cnx = Utilities.getConnection();
            PreparedStatement stmt = cnx.prepareStatement("SELECT * FROM Category ORDER BY id");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                Category category = populate(rs);
                categories.add(category);
            }
            cnx.close();
        } catch(SQLException ex){
            Logger.getLogger(Category.class.getName()).log(Level.SEVERE, null, ex);
        }
        return categories;
    }
    
}
