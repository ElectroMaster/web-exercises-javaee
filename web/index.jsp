<%-- 
    Document   : index
    Created on : 2019-2-20, 16:00:54
    Author     : exia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<!DOCTYPE html>
<html>
    <head>
        <title>Auctions List</title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <script src="js/main.js" type="text/javascript"></script>
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <!--fix one netbeans - chrome connection bug-->
        <link rel="shortcut icon" href="#" />
    </head>
    <body>
        <jsp:useBean id="auction" scope="page" class="org.centrale.prweb.item.Auction"></jsp:useBean>
        <h1>List of items</h1>
        <table id="myTable">
            <tr>
                <th>Auction #</th>
                <th>Auction type</th>
                <th>Seller</th>
                <th>Description</th>
                <th>Category</th>
                <th><input type="button" name="refresh" value="refresh" onclick="refresh();"></th>
            </tr>
        <c:forEach items="${auctions}" var="auction">
            <tr class="line">
                <td>${auction.id}</td>
                <td>${auction.title}</td>
                <td>${auction.author}</td>
                <td>${auction.body}</td>
                <!--<td><input type="button" name="delete" value="delete" onclick="deleteTheLine(this)"></td>-->
                <td>
                    <c:if test="${auction.category != null}">
                        ${auction.category.name} 
                    </c:if>
                </td>
                <td><input type="button" name="delete" value="delete" onclick="m_delete(this, ${auction.id});"></td>
            </tr>
        </c:forEach>
            <form action="add" method="POST">
                <tr id="addNew">
                    <td></td>
                    <td><input type="text" name="title" id="title" size="20" style="background-color: #e8e8e8"></td>
                    <td><input type="text" name="author" id="author" size="20" style="background-color: #e8e8e8"></td>
                    <td><input type="text" name="body" id="body" size="60" style="background-color: #e8e8e8"></td>
                    <td>
                        <select name="category">
                            <option value="-1"></option>
                        <c:forEach items="${categories}" var="category">
                            <option value="${category.id}">${category.name}</option>
                        </c:forEach>
                        </select>
                    </td>
                    <td><button>add</button></td>
                </tr>
            </form>
        </table>
    </body>
</html>
