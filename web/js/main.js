var mylist = [];
 
 
function deleteTheLine(o){
    var p = o.parentNode.parentNode;
    p.parentNode.removeChild(p);
}
 
 
 
function changeBackground(o, color){
    var tdRef = getMyTDTag(o);
    // tdRef.parentNode.className="backgroundRed"
    tdRef.parentNode.style.backgroundColor=color;
}
function backtoInit(o){
    var tdRef = getMyTDTag(o);
    // tdRef.parentNode.className="backgroundInit";
    tdRef.parentNode.style.backgroundColor="white";
}
 
 
 
function getMyTDTag(ref){
    var theTDRef = ref;
    var found = false;
    while((theTDRef!==null)&&(!found)){
        if (theTDRef.nodeType===3){
            theTDRef = theTDRef.parentNode;
        } else if (theTDRef.tagName === "TD"){
            found = true;
        } else {
            theTDRef=theTDRef.parentNode;
        }
    }
    return theTDRef;
}
 
function refresh(){
    $.ajax({
        url: "/prwebJ2EE2/refresh",
        method: "POST",
        success: function(result){
            removeLines();
//            console.log(result);
        //here result is a string , you need to parse it as json
            var res = $.parseJSON(result);
            console.log(res.data);
            var obj = res.data;
            for(var i=0; i<obj.length;i++){
                addNewLine(obj[i].id, obj[i].title, obj[i].author, obj[i].body, obj[i].category_name);
            }
            console.log("OK");
        },
        error: function(result, statut, erreur) {
            console.log("error");
        }
    });
    mylist = [];
}
 
function m_delete(theRef, id){
    $.ajax({
        url:"/prwebJ2EE2/delete",
        data: {
            "id":id
        },
        method: "POST",
        success: function(result){
            deleteTheLine(theRef);
            console.log("---------------");
            console.log("OK");
        },
        error: function(result, statut, erreur) {
            console.log("error");
        }
    });
}
 
function removeLines(){
    var listeTR = document.getElementsByTagName("TR");
    var toBeRemoved = new Array();
    for (var i=0;i<listeTR.length;i++){
        var ref = listeTR[i];
        if(ref.className==="line") {
            toBeRemoved.push(ref);
        }
    }
    while(toBeRemoved.length>0){
        ref = toBeRemoved.pop();
        ref.parentNode.removeChild(ref);
    }
}
function addNewLine(id, title, author, subject, category_name) {
    var string = "<tr class=\"line\">";
    string += "<td>"+id+"</td>";
    string += "<td>"+title+"</td>";
    string += "<td>"+author+"</td>";
    string += "<td>"+subject+"</td>";
    string += "<td>"+category_name+"</td>";
    string += "<td><input type=\"button\" name=\"delete\" value=\"delete\" onclick=\"deleteTheLine(this)\"></td>";
    string += "</tr>";
    $('#myTable tr:last').before(string);
}
